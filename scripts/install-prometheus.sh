#!/bin/bash

# THIS SCRIPT MUST BE RUN FROM IT'S DIRECTORY LOCATION

#create secureci-prometheus namespace
kubectl create ns secureci-prometheus

mkdir -p ../addons/prometheus/
cd ../addons/prometheus
helm repo update
helm fetch stable/prometheus-operator --version 8.7.0
cd ../../scripts
helm install --name secureci-prometheus --namespace secureci-prometheus ../addons/prometheus/prometheus-operator-8.7.0.tgz

kubectl wait -n secureci-prometheus --for=condition=Ready pod --all 
