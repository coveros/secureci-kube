#!/bin/bash

# Wait until docker installs
while [ ! -f docker.done ] ; do echo "waiting on docker install"; sleep 1; done

echo 'Configure iptables forward...'
iptables -P FORWARD ACCEPT
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
apt-get -y install bash-completion awscli zip golang-go jq

touch  packages.done

