export HOME=/root
export GOHOME=/root/go

while [ ! -f packages.done ] ; do echo "waiting on package install"; sleep 1; done

go get github.com/pelletier/go-toml/cmd/tomljson
go install github.com/pelletier/go-toml/cmd/tomljson

touch toml-parser.done
