#!/bin/bash

nexusUrl=http://$1:$2

echo "Nexus repository creation script start using URL $nexusUrl..."

until wget --timeout=120 --spider $nexusUrl/index.html;
do
  echo 'Nexus service not ready trying again...'
  sleep 2;
done

echo 'Nexus service is ready. Creating repos...'

curl -v -u admin:admin123 -X POST "$nexusUrl/service/rest/v1/script" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"name\": \"createRepos\", \"type\": \"groovy\", \"content\": \"repository.createDockerHosted('docker', 8084, null);log.info('helloworld docker repo created successfully');repository.createRepository(repository.createHosted('helm', 'helm-hosted'));log.info('helloworld helm repo created successfully');\"}"

curl -v -X POST -u admin:admin123 --header "Content-Type: text/plain" "$nexusUrl/service/rest/v1/script/createRepos/run"
