#!/bin/bash

IP_PORT=$1

jenkins_url="http://$IP_PORT/login"

echo -n "Checking Jenkins availability ($jenkins_url)..."

while [[ "$(curl -s -o /dev/null -w ''%{http_code}'' $jenkins_url)" != "200" ]];
do
  echo -n ".";
  sleep 3;
done

echo ""
echo "Jenkins is up and running and accessible at $jenkins_url"
# until wget --no-verbose --timeout=600 --wait=5 --spider $jenkinsUrl;
# do
#   echo -n '.'
# done
