#!/bin/bash

# Wait until docker installs
while [ ! -f docker.done ] ; do echo "waiting on docker install"; sleep 1; done

#build test docker image
docker build -t localhost:32000/testcontainer:latest -f testcontainers/DockerfileChrome testcontainers
docker tag localhost:32000/testcontainer:latest jenkins-demo-sonatype-nexus:32003/testcontainer:latest 

touch createtestcontainer.done
