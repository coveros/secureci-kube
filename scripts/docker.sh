#!/bin/bash

dist_version="$(lsb_release --codename | cut -f2)"
curl -fsSL https://get.docker.com -o get-docker.sh

# this is until docker fixes the 19.10 repo
if [ "${dist_version}" = "eoan" ] ; then
    sed -i.bak -e 's/$dist_version $CHANNEL/disco $CHANNEL/g' get-docker.sh
fi

sudo sh get-docker.sh
echo '{ "insecure-registries" : ["localhost:32000", "jenkins-demo-sonatype-nexus:8080", "192.168.1.150:8083" ] }' | sudo tee -a /etc/docker/daemon.json
sudo systemctl restart docker
sudo usermod -aG docker ubuntu
# TODO: hack to fix permission denied issue. Figure out group that worker node
# is running as and add to docker group
echo 'Workaround for Docker permission issue...'
sudo chmod 777 /var/run/docker.sock

touch docker.done
