helm delete --purge secureci-prometheus
kubectl delete crd prometheuses.monitoring.coreos.com -n secureci-prometheus
kubectl delete crd prometheusrules.monitoring.coreos.com -n secureci-prometheus
kubectl delete crd servicemonitors.monitoring.coreos.com -n secureci-prometheus
kubectl delete crd podmonitors.monitoring.coreos.com -n secureci-prometheus
kubectl delete crd alertmanagers.monitoring.coreos.com -n secureci-prometheus

kubectl delete namespace secureci-prometheus
