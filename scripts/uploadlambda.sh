#!/bin/bash
if [ -z "$1" ] ; then echo "enter first.lastname"; exit 1; fi
rm -rf ../Deploy.zip
zip ../Deploy.zip *
aws --region us-east-1 lambda update-function-code --function-name microservice_serverless_handson --zip-file fileb://../Deploy.zip
VERSION=`aws --region us-east-1 lambda publish-version --function-name microservice_serverless_handson | grep Version | sed -e 's/ *"//g' | sed -e 's/Version://g' | sed -e 's/,//g'`
aws --region us-east-1 lambda create-alias --function-name microservice_serverless_handson --name $1 --function-version ${VERSION}
