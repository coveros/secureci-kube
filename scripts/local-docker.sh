#!/bin/bash

dist_version="$(lsb_release --codename | cut -f2)"
curl -fsSL https://get.docker.com -o get-docker.sh

sed -i.bak -e 's/apt_repo=.*/apt_repo="deb http:\/\/192.168.1.150:8081\/repository\/docker-apt disco stable"/g' get-docker.sh
chmod 755 get-docker.sh

sudo sh get-docker.sh
echo '{ "insecure-registries" : ["localhost:32000", "jenkins-demo-sonatype-nexus:8080", "192.168.1.150:8083" ] }' | sudo tee -a /etc/docker/daemon.json

cat <<EOF >> /etc/containerd/config.toml
[plugins.cri.registry.mirrors]
  [plugins.cri.registry.mirrors."registry-1.docker.io"]
    insecure_skip_verify = true
    endpoint = ["http://192.168.1.150:8083"]
  [plugins.cri.registry.mirrors."192.168.1.150:8083"]
    insecure_skip_verify = true
    endpoint = ["http://192.168.1.150:8083"]
EOF

sed -i.bak -e 's/--containerd=\/run\/containerd\/containerd.sock/--containerd=\/run\/containerd\/containerd.sock --registry-mirror http:\/\/192.168.1.150:8083/g' /usr/lib/systemd/system/docker.service

systemctl daemon-reload
sudo systemctl restart containerd
sudo systemctl restart docker
sudo usermod -aG docker ubuntu
# TODO: hack to fix permission denied issue. Figure out group that worker node
# is running as and add to docker group
echo 'Workaround for Docker permission issue...'
sudo chmod 777 /var/run/docker.sock

sed -i.bak -e 's/http:\/\/dl.google.com\/linux\/chrome\/deb\//http:\/\/192.168.1.150:8081\/repository\/chrome\//g' testcontainers/DockerfileChrome
head -2 testcontainers/DockerfileChrome >> testcontainers/DockerfileChrome.tmp
echo "RUN sed -i.bak -e 's/http:\/\/deb.debian.org\/debian stretch/http:\/\/192.168.1.150:8081\/repository\/debian-main stretch main/g' /etc/apt/sources.list" >> testcontainers/DockerfileChrome.tmp
echo "RUN sed -i.bak -e 's/http:\/\/deb.debian.org\/debian stretch-updates/http:\/\/192.168.1.150:8081\/repository\/debian-updates stretch/g' /etc/apt/sources.list" >> testcontainers/DockerfileChrome.tmp
tail -7 testcontainers/DockerfileChrome >> testcontainers/DockerfileChrome.tmp
cp testcontainers/DockerfileChrome.tmp testcontainers/DockerfileChrome
rm -f testcontainers/DockerfileChrome.tmp

touch docker.done
