#!/bin/bash
JENKINS_POD=`kubectl -n default get pods | egrep "jenkins-demo-[^-]*-[^-]* " | sed -e 's/ .*//g'`
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/angular-ui /var/jenkins_home/jobs/angular-ui/config.xml"
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/auth-service /var/jenkins_home/jobs/auth-service/config.xml"
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/catalog-service /var/jenkins_home/jobs/catalog-service/config.xml"
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/deployment /var/jenkins_home/jobs/deployment/config.xml"
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/gateway /var/jenkins_home/jobs/gateway/config.xml"
kubectl exec -ti $JENKINS_POD -- bash -c "cp /var/jenkins_jobs/user-service /var/jenkins_home/jobs/user-service/config.xml"
