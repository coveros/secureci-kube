#!/bin/bash

# THIS SCRIPT MUST BE RUN FROM IT'S DIRECTORY LOCATION

# Install helm
kubectl apply -f helm/helm-rbac.yaml
helm init --service-account=tiller --history-max 300

# Create SecureCI Namespace
kubectl create ns secureci

# Create jenkins nexus credentials
kubectl delete secrets --wait jenkins-credentials --namespace secureci
kubectl create secret generic jenkins-credentials --from-file=../credentials.xml --namespace secureci

# Create role/role mappings
kubectl create rolebinding jenkins-admin-role-binding --role=api-pod-reader --serviceaccount=default:default --namespace=secureci
kubectl create clusterrolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=default:default --namespace=secureci

# Create configmap for provisioning script
kubectl delete configmaps wrapper --namespace secureci
kubectl create configmap wrapper --from-file=provision-nexus.sh --namespace secureci

# Wait for tiller pod ready
kubectl -n kube-system wait --for=condition=Ready pod -l name=tiller --timeout=300s

#create service accounts
kubectl apply -f secureci-rbac/secureci-admin.yaml

# Install chart
helm init
cd ../charts/charts
helm repo update
helm fetch stable/postgresql --version 7.7.3
helm fetch stable/jenkins --version 1.9.4
cd ../../scripts
helm install --name secureci --namespace secureci --set gitlab-ce.externalUrl=http://$HOSTNAME.com ../charts

#Wait 300 seconds for the pods to enter "Ready" state
kubectl wait -n secureci --for=condition=Ready pod --all --timeout=300s
