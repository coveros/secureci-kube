#!/bin/bash
#set -xv

# THIS SCRIPT MUST BE RUN FROM /home/ubuntu/securecidemo-jenkins-k8s/scripts

echo 'Install microk8s...'
snap install --channel=1.16/stable microk8s --classic
snap alias microk8s.kubectl kubectl


# Use a toml parser to configure microk8s to allow nexus as an untrusted registry
while [ ! -f  toml-parser.done ] ; do echo "waiting on toml-parser install"; sleep 1; done
export HOME=/root
export GOHOME=/root/go

cp /var/snap/microk8s/current/args/containerd-template.toml /var/snap/microk8s/current/args/containerd-template.toml.bak
MICROK8S_FILE=`cat /var/snap/microk8s/current/args/containerd-template.toml.bak  | go run /root/go/src/github.com/pelletier/go-toml/cmd/tomljson/main.go | jq '.plugins.cri.registry.mirrors = null' | jq '.plugins.cri.registry.mirrors."\"jenkins-demo-sonatype-nexus:8081\"".endpoint = [ "http://jenkins-demo-sonatype-nexus:8081" ]' | jq '.plugins.cri.registry.mirrors."\"jenkins-demo-sonatype-nexus:32003\"".endpoint = [ "http://jenkins-demo-sonatype-nexus:32003" ]' | jq '.plugins.cri.registry.mirrors."\"docker.io\"".endpoint = [ "https://registry-1.docker.io" ]' | jq '.plugins.cri.registry.mirrors."\"local.insecure-registry.io\"".endpoint = [ "http://localhost:32000" ]'`; echo ${MICROK8S_FILE} |  go run /root/go/src/github.com/pelletier/go-toml/cmd/jsontoml/main.go | sed -e 's/\.[0-9]$//g' > /var/snap/microk8s/current/args/containerd-template.toml


# Enables public access to Kubernetes API Server
#kubectl proxy --accept-hosts="^*$" --address='<PRIVATE IP>'

#echo 'Configure microk8s allow-privileged...'
#sudo echo '--allow-privileged=true' >> /var/snap/microk8s/current/args/kubelet
# sudo systemctl restart snap.microk8s.daemon-kubelet.service
#sudo echo '--enable-admission-plugins="NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota,PodSecurityPolicy"' >> /var/snap/microk8s/current/args/kube-apiserver
#sudo echo '--allow-privileged=true' >> /var/snap/microk8s/current/args/kube-apiserver
# sudo systemctl restart snap.microk8s.daemon-apiserver.service

echo "Restarting microk8s to allow for default registry change"
/snap/bin/microk8s.stop
/snap/bin/microk8s.start
/snap/bin/microk8s.status --wait-ready


echo 'Enable microk8s services...'
/snap/bin/microk8s.enable dns registry dashboard helm

# Create jenkins nexus credentials
/snap/bin/microk8s.kubectl delete secrets --wait jenkins-credentials
/snap/bin/microk8s.kubectl create secret generic jenkins-credentials --from-file=../credentials.xml

# Create role/role mappings
/snap/bin/microk8s.kubectl create rolebinding jenkins-admin-role-binding --role=api-pod-reader --serviceaccount=default:default --namespace=default
/snap/bin/microk8s.kubectl create clusterrolebinding jenkins-admin-binding --clusterrole=admin --serviceaccount=default:default --namespace=default

# Create configmap for provisioning script
/snap/bin/microk8s.kubectl delete configmaps wrapper
/snap/bin/microk8s.kubectl create configmap wrapper --from-file=provision-nexus.sh

# Create GitLab deploy key secrets
/snap/bin/microk8s.kubectl apply -f ../gitlab-secrets.yaml


# Wait for tiller pod ready
export HOME=/home/ubuntu
export HELM_HOME=/home/ubuntu/.helm
/snap/bin/microk8s.helm init

sleep 30;
/snap/bin/microk8s.kubectl -n kube-system wait --for=condition=Ready pod -l name=tiller --timeout=300s

# Install chart
echo "Getting charts"
cd ../charts/charts
/snap/bin/microk8s.helm fetch stable/postgresql --version 7.7.1
/snap/bin/microk8s.helm fetch stable/jenkins --version 1.9.4
#/snap/bin/microk8s.helm fetch stable/sonatype-nexus --version 1.21.2
cd ../../scripts

echo "Deploying CI helm charts" 
/snap/bin/microk8s.helm install --name jenkins-demo ../charts

# Wait until docker installs
while [ ! -f createtestcontainer.done ] ; do echo "waiting on test container creation install"; sleep 1; done


# Wait for registry pod ready
/snap/bin/microk8s.kubectl -n container-registry wait --for=condition=Ready pod -l app=registry --timeout=300s

#Push test image
docker push localhost:32000/testcontainer:latest

# Wait for nexus
STATUS="000"; while [ ${STATUS} != "200" ] ; do STATUS=`curl -s -o /dev/null -w ''%{http_code}'' http://localhost:32003 || true`; echo "Nexus STATUS: ${STATUS}"; sleep 5; done
docker login -u admin -p admin123  http://jenkins-demo-sonatype-nexus:32003
docker push  jenkins-demo-sonatype-nexus:32003/testcontainer:latest

echo "Finished Microk8s setup"
touch microk8s.done
