#create istio namespace
kubectl create ns istio-system 

#make sure tiller service account is a cluster admin
kubectl apply -f ../addons/istio-1.4.4/install/kubernetes/helm/helm-service-account.yaml

#helm install istio crds
helm install ../addons/istio-1.4.4/install/kubernetes/helm/istio-init --name istio-init --namespace istio-system

#wait for CRDs to be created
kubectl -n istio-system wait --for=condition=complete job --all

#install istio
helm install ../addons/istio-1.4.4/install/kubernetes/helm/istio --name istio --namespace istio-system \
    --values ../addons/istio-1.4.4/install/kubernetes/helm/istio/values-istio-demo.yaml
