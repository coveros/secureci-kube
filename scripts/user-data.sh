#!/bin/bash


echo 'Configure SCM access...'

echo 'Host gitlab.com
    Hostname gitlab.com
    User git
    IdentityFile ~/.ssh/id_rsa_jenkinsk8sdemo' \
    >> ~/.ssh/config

echo '-----BEGIN RSA PRIVATE KEY-----
MIIJJwIBAAKCAgEA/RrW0AyHwhw7Mir8aSspE1CoZZqMtr/+TV7zT27g4yGoFQHh
kEW9K3Yf6iQxh/4KTWZDLVPE71bqJZ3eopEwDW2nwGiJoB02OMbPFYIk5cfpp2db
HYfveWsxQWzCd3wvidiKA1DWBXzI2sOd116Ywh7umjs4HbN34aJPxBIa7eGCeBN3
Kexiu4x0hHVY8sSplx82AENHH7zny8CGhGlWiNzZ4qDSl0gj8Pw0WPwpHMBMq7dy
0yjr16lROgMnzo4ymaQo5T4LAeVUtV7PJ99hYqAQuk47yKZUjXCoOwNSDL9lLPbA
sNeF+mo/sEnjnk2KzKHbWH8/j8Y1fK64GiLwpNDQD3jQykuMyW3r49d7AazSqlIr
I+4wDauH8s/e+mr1m5CQuRP/d7VkiLhKS+eBEy+mTjs6ITR5KUMU+RqjcGsQDPI6
W1htGXSlVVuUkh3azc+qAnZd2u4WnwWdZf33FHOtcNVgsgeaNit7anx1suJ9fP/r
CrIy6WhV3HVDE+eIxh37qHRj38nePze3JesVeKPbnMTiCDlFLcDMViGDKQXO0fnV
prXDggZD+TcsyV7bGeygdzN6Zpurr9cyr9nYXVP6WSHq4zLEwClqB/tHwXiBrPij
0klKL1UKi/np+wk8tM/l8UqIPRoDkMNgeteG1E9MDbdXXCgH+VHGR9G+Ky8CAwEA
AQKCAgA7g34TOX8Kg3zf1P+48u8bnZGbInYH2qk/JB5uBRJYFefh2KljNSK//2yN
r6j94xwlCvCszc+2v2cbJT3eV+lGnwwcz/fccEH5o7W5xvpKFXG5bTIdhYMnt67D
RYGMeuTqssHw+ab5b1gDKJAKvjGJI6DTHy2YvkRoB7Rd1CwE10vIVDtm3jBxDRuw
sa8Slm+SG0KlWSx2KAMRA9oddG9mixeolEwwSkQ5O/RLIC/ClOzyS5ZyVGLwON3i
mC9aAFGhktoS3T3VWklKxFpNc9cAS8bjJ9Yi3glvoJfOGH/Ig5CUvAifDG5ybV1A
ZH9jD7SdXUmoS3N0qRSWlravXpvajZGUh864mtHC7uWPMIiWes0DM1u3ZlxxlGTm
WcVqZgXH1tXMbEpZQf0U5mILtTYrSAxZqXhKuyBupmaykzR/HoVq16g+KJ25LsaT
1DJtQ5lKPPbKofRYN9qebICYKBMtvYL/IMgv2SIgt3NqFc8rK87eOfmlYubQnxex
daXbdq28nrAUB0Pd87Rw+jylrrnDsizywnbX53j9+qJ8hpFfzuotteFmH9pNqCyC
bWQNV4VDVEM2hRcC6aMPAcXO8DIkVgtT7838mjcSu68oDYmwipWIHzdfysSNDD97
Q98L1FCOX7nHwgUcPde4LrWUjMedSGVQlQOaUVUqCcFSHcqugQKCAQEA/29cJOcd
HMLISFWCH4dWb7+PMtSHXs3WAVsrCCx3lqJ9KavqNcYQx6VmVshYf5FqEX0F5TtO
qByUCSXLlMerrNDxu6jHpuWWgrlBMbyZZw6/Uih85uhZhjPZcCUvxvX8tzgU32ph
g4FjuSrkgNNjThh/mXJTIOdXMYajibICtRasDlAx3b1LLW3kAnBFsbsPviacNDmJ
fak6d45OOufqpR2hBnjMSQelorM6nCA95LT32v+nS2KzmSFJLlMgANI0vTaH1DKJ
WfRMgTmQtGcCXTk0uSJxiY9EoqKeBayUdlIZCi9YRIEhwRNUgxlv0sdnOKxeZ2a+
CYP3IP6ep8yTjwKCAQEA/aoo433BF6ADEB2j1cqvd+RCJ+UrD8SypoMeySE/bMJC
X+OoU0wEAwFN2StDykBjvXLqFD5YfReLcbrufxAPQBTjPn3Png+0cTY6MYOwLt0a
lHsMrL9UN1z4KszCR/XMmkHqgB+z3t5ZJf5/hFW15aEgeNjhnx0QVqJK8G3L+ugQ
FiuZ0W2DPc6a0rQ4pYsk4ufJuFXsovDzD0o0ibXiTlGgDf2puqPH4Y1x/XQ6ezO9
xRoBLK1YbWSuA3b9jqXt+gOcOWM4ptDVsSD0QioKOWJGIc95O8mJ9xLZEPD0WzmB
wJgSiAYBjIEpxVCoTPF+1Jhs7ELk8JLnZVHQL1ieYQKCAQBw06uL0VoWniMBcGOt
sYYIjkCeXyn8QkdJChAT6DBapSNMCNfHMNmwleqCYrRUhNKpLuQvwOaZcyYgdEVc
TmYSS6i7D25A1iCJVXLqmTfZQbZYf/HLvM3ubXCaDJLx+y3JcEgn90FjaP+3hq6w
CK9Qi5mxGcKGQilNgv8xjyOLAZy2Jc4g98ABLQ0N1rGyM8JcZ4mPt+p3Swy8Smtb
sZz/mfrKdQknKO2M1EYLOGKeZyEj5tDqh4p1FTLR932nXzJUcYXFwRzSUwqH+flk
9ishKmeTbxdff5z9iX7rWrOpH9JXoVoHyFI0imlGDzJJlDKP4fNBESzX3yaFuHI0
BDUZAoIBAA8WYdw2DJgVIlmBCc+bgJhG8VBiJw+HCYEx51DbM2SKoVwGtGpP6SUs
nK7ADgYnkkcgYJ/XnXfDQL4A+lKAaAfNtj5vSTwDfx4Eta1gQchnyj7umrBjqOSq
GPCiwSN6kTLTnNkGCbLF0tOZsYfzeSAxVzcY30KRfflJKd/nB+zihKJs8XctZlL0
yOyjX4MpJYaBdtQJ+EswvaQ5K6rNMKqr3DM41TGOsELQIOmMUjYlu7HPfCZmIQ5Q
oLEAENC9OcxXDqGzLxls7mjcpuqQiY8kMzurlXBi6lvRuStyyXXrk7Ya1W9VvB3Z
SNy9kWUSMBMBBel/iRJuCmlSVmmagcECggEAOtx5A615x+sBl1bKD7kn8jF3MR0q
spGFtHUcsTXwbA5sawjTKXQBnZk8MmjK39MmrdOiNsBo6QXwWClGSWB8nXZC3KkE
9TI4nyjlHTZcXBVIbOWGX6gpEgGEEy4gLKzUA4uvHh+mWG8m+gTjFKgiDbsVkmj2
o+4Kd5Yw61qJEvs12TVwjtF5DJ7CHadOTwxLagkutYExLZphj0vjptiijcDMY7Ar
U8irT90Ufb+SudXuiOtpJI9kiKaElH47Od43r5M0VdngCsEALdvY5BZXCerPhwBH
HshRAph8iIEjSmyvktLiGAUMvle6fWlPShdKGqzIuyTRpztL7cmPCOXhgQ==
-----END RSA PRIVATE KEY-----' \
> ~/.ssh/id_rsa_jenkinsk8sdemo

sudo chmod 600 ~/.ssh/id_rsa_jenkinsk8sdemo

echo '|1|MvCkxsKsDclHbh5Sz+OFY2IabBA=|wnWkcAxkplBXYpTX6ROYLrJF0Mk= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=
|1|oB55zE63+/d8gqMMGihOM4pvAbY=|hY+3N8rQEa6Xyfl7wQlOtjaaywM= ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBFSMqzJeV9rUzU4kWitGjeR4PWSa29SPqJ1fVkhtj3Hw9xjLVXVYrU9QlYWrOLXBpQ6KWjbjTDTdDkoohFzgbEY=' \
>> ~/.ssh/known_hosts

echo 'Checkout of repository...'
rm -rf /home/ubuntu/securecidemo-jenkins-k8s
git clone --depth 1 git@gitlab.com:coveros/securecidemo-jenkins-k8s.git /home/ubuntu/securecidemo-jenkins-k8s

# Set the lamda password in the chart
sed -i.bak -e "s/<<LAMBDAPASS>>/<<LAMBDA_USER_SH>>/g" /home/ubuntu/securecidemo-jenkins-k8s/charts/values.yaml

#Set the alias for the docker registry so nexusProxy routes us to the right place. 
sed -i.bak -e 's/127.0.0.1.*/127.0.0.1 localhost jenkins-demo-sonatype-nexus/g' /etc/hosts

echo 'Configure iptables forward...'
apt-get update
iptables -P FORWARD ACCEPT
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
apt-get -y install iptables-persistent

cd /home/ubuntu/securecidemo-jenkins-k8s/scripts
 
/home/ubuntu/securecidemo-jenkins-k8s/scripts/docker.sh &
/home/ubuntu/securecidemo-jenkins-k8s/scripts/createtestcontainer.sh &
/home/ubuntu/securecidemo-jenkins-k8s/scripts/packages.sh &
/home/ubuntu/securecidemo-jenkins-k8s/scripts/toml-parser.sh &
/home/ubuntu/securecidemo-jenkins-k8s/scripts/microk8s.sh &
/home/ubuntu/securecidemo-jenkins-k8s/scripts/file_permission_moves.sh &

while [ ! -f createtestcontainer.done -o ! -f docker.done -o ! -f microk8s.done -o ! -f file_permission_moves.done -o ! -f packages.done -o ! -f toml-parser.done ] ; do  echo "Waiting for threads to join"; ls *.done; sleep 1; done

rm -f docker.done
rm -f toml-parser.done
rm -f microk8s.done
rm -f file_permission_moves.done
rm -f packages.done
rm -f createtestcontainer.done
