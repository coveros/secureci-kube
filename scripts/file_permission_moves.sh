#!/bin/bash

#quick way to change repositories for a class or other reason
cp /home/ubuntu/securecidemo-jenkins-k8s/scripts/configure.sh /home/ubuntu
cp /home/ubuntu/securecidemo-jenkins-k8s/scripts/revert.sh /home/ubuntu

#put the upload lambda scrupt in the right place
mkdir /home/ubuntu/lambda
cp /home/ubuntu/securecidemo-jenkins-k8s/scripts/uploadlambda.sh /home/ubuntu/

echo 'Generating User SSH key'
ssh-keygen -b 2048 -t rsa -f /tmp/sshkey -q -N ""
cat /tmp/sshkey.pub >> /home/ubuntu/.ssh/authorized_keys
mv /tmp/sshkey /home/ubuntu/sshkey
chown -R ubuntu:ubuntu /home/ubuntu/
rm -f /tmp/sshkey.pub

echo 'Adding retrieval key'
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJR+lDs7b0dLFRmkbJyFxlgNoRGfulQy7UaPHxK6zcBAicziUmAGmdpdnkOTg9YOpX4bNnYfu4z18Go7G051K22BjAURw038+H3a7n5t957kHShb+swZwBK6HHy7ItJE5AFZQFKRpsDlHXeDqnLZJov1dFjnV9ult/JT0piyOC0PLNhIRTkeeM24OSjj4C/13HacJbTc06YBDKcoYMqzOI9QdfvVP5TegOW+vINlpYLCPsdqDyV0FXr12Ys+NEwhP8biaFRKHwzIO7pMtqbSgAhLZE6u6fPj5fkS1RSNACAUMDbTsRsHUqA5wQb72AvNXp6MIXglP0m6op4YCqenDd glenn.buckholz@secureci" >> /home/ubuntu/.ssh/authorized_keys


chown -R ubuntu /home/ubuntu/
chgrp -R ubuntu /home/ubuntu/

touch file_permission_moves.done

