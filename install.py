import re
import os
import argparse
import logging
import sys
import subprocess
import shutil

log = logging.getLogger("terraform-logger")
parser = argparse.ArgumentParser()
parser.add_argument('--addons', nargs='+',
                   help='Addons to install')
parser.add_argument('--debug', action='store_true',
                    help='Enable debugging output')
parser.add_argument('--create', action='store_true',
                    help='Enable debugging output')
args = parser.parse_args()

#set logging level
if args.debug:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
else:
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
   
#grab our variables from the parser
addons = args.addons

available = []
for file in os.listdir(os.getcwd() + "/scripts"):
  if file.startswith("install-"):
    temp = re.search('install-(.*).sh', file)
    feature = temp.group(1)
    if feature != 'secureci':
      available.append(feature)
if args.create:
  os.chdir('terraform')
  try:
    init = subprocess.check_output("terraform init", shell=True)
    init = init.decode("utf-8")
    logging.debug(init)
    logging.info("Terraform init successful")
  except subprocess.CalledProcessError as e:
    logging.error(e)
    logging.error("Terraform init failed")
    sys.exit(1)

#real time logging
  f = open("kube-config", "w")
  kubeconfig = False
  sub_process = subprocess.Popen("terraform apply -auto-approve -no-color", close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
  while sub_process.poll() is None:
      out = sub_process.stdout.readline().decode("utf-8")
      if kubeconfig:
        f.write(out)
      if "kubeconfig =" in out:
          kubeconfig = True
      logging.debug(out)
    
  logging.info("Terraform apply successful")
  logging.info("Kube config written to ./terraform/kube-config")
  f.close()
  shutil.copyfile('kube-config', os.environ['HOME'] + '/.kube/config')

os.chdir('../scripts')
logging.info("Installing SecureCI Base")
secureci_process = subprocess.Popen("./install-secureci.sh", close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
while secureci_process.poll() is None:
  out = secureci_process.stdout.readline().decode("utf-8")
  logging.debug(out)

if secureci_process.returncode is 1:
  logging.error(secureci_process.stderr.read().decode("utf-8"))
  sys.exit(1)
else:
  logging.info("SecureCI Base installed successfully")

if addons is not None:
  logging.info("Installing Addons")
  for i in addons:
    if i in available:
      logging.info("Installing " + i)
      addon_subprocess = subprocess.Popen("./install-"+i+".sh", close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      while addon_subprocess.poll() is None:
        out = addon_subprocess.stdout.readline().decode("utf-8")
        logging.debug(out)

      if addon_subprocess.returncode is 1:
        logging.error(addon_subprocess.stderr.read().decode("utf-8"))
        sys.exit(1)
      else:
        logging.info("Addon " + i + " installed successfully")
    else:
      logging.info(i + " not available")
