import os
import logging 
import sys
import subprocess
import argparse

log = logging.getLogger("terraform-logger")
#logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--debug', action='store_true')
args = parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
else:
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    
            
os.chdir('terraform')
try:
  init = subprocess.check_output("terraform init", shell=True)
  init = init.decode("utf-8")
  logging.debug(init)
  logging.info("Terraform init successful")
except subprocess.CalledProcessError as e:
  logging.error(e)
  logging.error("Terraform init failed")
  sys.exit(1)

#real time logging
#kubeconfig = 
f = open("kube-config", "w")
kubeconfig = False
sub_process = subprocess.Popen("terraform apply -auto-approve", close_fds=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
while sub_process.poll() is None:
    out = sub_process.stdout.readline().decode("utf-8")
    if "kubeconfig =" in out:
        kubeconfig = True
    if kubeconfig:
      f.write(out)
    logging.debug(out)
    
logging.info("Terraform apply successful")
f.close()