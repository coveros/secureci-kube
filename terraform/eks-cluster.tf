#
# EKS Cluster Resources
#  * IAM Role to allow EKS service to manage other AWS services
#  * EC2 Security Group to allow networking traffic with EKS cluster
#  * EKS Cluster

resource "aws_iam_role" "secureci-role" {
  name = "${var.cluster-name}-cluster-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "secureci-cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.secureci-role.name
}

resource "aws_iam_role_policy_attachment" "secureci-cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.secureci-role.name
}

resource "aws_security_group" "secureci-sg" {
  name        = "${var.cluster-name}-cluster-sg"
  description = "Cluster communication with worker nodes"
  vpc_id      = var.vpc-id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.cluster-name}-cluster-security-group"
  }
}

resource "aws_security_group_rule" "secureci-cluster-ingress-workstation-https" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allow workstation to communicate with the cluster API Server"
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.secureci-sg.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_eks_cluster" "secureci" {
  name     = var.cluster-name
  role_arn = aws_iam_role.secureci-role.arn
  version  = var.cluster-version

  vpc_config {
    security_group_ids = [aws_security_group.secureci-sg.id]
    subnet_ids         = var.subnet-ids
  }

  depends_on = [
    aws_iam_role_policy_attachment.secureci-cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.secureci-cluster-AmazonEKSServicePolicy,
  ]
}
