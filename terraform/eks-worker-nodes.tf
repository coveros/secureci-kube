#
# EKS Worker Nodes Resources
#  * IAM role allowing Kubernetes actions to access other AWS services
#  * EKS Node Group to launch worker nodes
#

resource "aws_iam_role" "secureci-node" {
  name = "${var.cluster-name}-node"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "secureci-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.secureci-node.name
}

resource "aws_iam_role_policy_attachment" "secureci-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.secureci-node.name
}

resource "aws_iam_role_policy_attachment" "secureci-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.secureci-node.name
}

resource "aws_eks_node_group" "secureci" {
  cluster_name    = aws_eks_cluster.secureci.name
  node_group_name = var.nodegroup-name
  node_role_arn   = aws_iam_role.secureci-node.arn
  subnet_ids      = var.subnet-ids
  instance_types  = var.nodegroup-instance-type

  scaling_config {
    desired_size = var.desired-size
    max_size     = var.max-size
    min_size     = var.min-size
  }

  depends_on = [
    aws_iam_role_policy_attachment.secureci-node-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.secureci-node-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.secureci-node-AmazonEC2ContainerRegistryReadOnly,
  ]
}
