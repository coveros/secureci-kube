#
# Variables Configuration
#

variable "cluster-name" {
  default = "secureci"
  type    = string
}

variable "cluster-version" {
  default = "1.15"
  type    = string
}

variable "cluster-role" {
  default = "secureci-cluster-role"
  type    = string
}

#coverosdev default vpc
variable "vpc-id" {
  default = "vpc-59c26f3e"
  type    = string
}

#subnet-14bbf03e = dev1
#subnet-af02dff5 = intern-project
variable "subnet-ids" {
  type    = list(string)
  default = ["subnet-af02dff5", "subnet-14bbf03e"]
}

variable "nodegroup-name" {
  default = "secureci-nodes"
  type    = string
}

variable "desired-size" {
  default = 2
  type    = number
}

variable "max-size" {
  default = 3
  type    = number
}

variable "min-size" {
  default = 2
  type    = number
}

variable "nodegroup-instance-type"{
  type    = list(string)
  default = ["r5a.large"]
}