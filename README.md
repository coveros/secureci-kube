In order to set up terraform creds follow this link:
https://hackernoon.com/introduction-to-aws-with-terraform-7a8daf261dc0

To create an EKS cluster, and install all of secureci and it's addons:
python3 install.py --create --debug --addons istio prometheus vault

To destroy an EKS cluster:
cd terraform/
terraform destroy -auto-approve

