import argparse
from pathlib import Path
import os
import subprocess
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--dir', type=str, required=True,
                   help='full path to helm chart directory')

args = parser.parse_args()

dir = args.dir

charts = ""
entries = Path(dir)
for entry in entries.iterdir():
    if entry.is_dir():
        charts = charts + " /home/" + entry.name 
        
docker_string = "docker run --rm -i -v " + dir + ":/home/ lachlanevenson/k8s-helm:v2.16.3 lint" + charts

proc = subprocess.run(docker_string,
  stdout = subprocess.PIPE,
  stderr = subprocess.PIPE,
  shell = True
)
#stdout, stderr = proc.communicate()
stdout = proc.stdout.decode().strip()
stderr = proc.stderr.decode().strip()

if stderr:
  for finding in stdout.split("==>"):
      for i in finding.split("\n"):
            if "[ERROR]" in i:
                  temp = finding.strip()
                  temp = temp.replace('Linting /home/', '')
                  print("ERROR in Chart: ", temp, " \n")
  sys.exit(1)