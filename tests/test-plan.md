Kubernetes Native SecureCI Test Plan

Core Components:
- Jenkins
- Gitlab
  - Postgres
  - Redis
- Sonarqube
  - Postgres
- Sonatype Nexus
- ZAP

Cloud Native Addons:
- Istio
- Prometheus
- Hashicorp Vault

Pre deployment testing:
- lint helm charts
- test bash cli

During deployment testing:
- ensure pods become "Ready"
  - use kubectl wait where possible

Post deployment testing:
- functional UI testing through Selenified
- api level testing through Selenified
