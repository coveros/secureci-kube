import com.coveros.selenified.Selenified;
import com.coveros.selenified.application.App;

import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class SonarTest extends Selenified{
    @BeforeClass(alwaysRun = true)
    public void beforeClass(ITestContext test) {
        // set the base URL for the tests here
        setTestSite(this, test, System.getProperty("ip") + ":9000");
    }
    @Test(groups = {"smoke", "sonar"}, description = "Make sure SonarQube is up")
    public void sonarHomeTest() {
        // use this object to manipulate the app
        App app = this.apps.get();
        // verify the correct page title
        app.azzert().titleEquals("SonarQube");
        // verify no issues
        finish();
    }
}