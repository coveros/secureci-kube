import com.coveros.selenified.Selenified;
import com.coveros.selenified.application.App;

import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class NexusTest extends Selenified{
    @BeforeClass(alwaysRun = true)
    public void beforeClass(ITestContext test) {
        // set the base URL for the tests here
        setTestSite(this, test, System.getProperty("ip") + ":8081");
    }
    @Test(groups = {"smoke", "nexus"}, description = "Make sure nexus is up")
    public void nexusHomeTest() {
        // use this object to manipulate the app
        App app = this.apps.get();
        // verify the correct page title
        app.azzert().titleEquals("Welcome - Nexus Repository Manager");
        // verify no issues
        finish();
    }
}