import com.coveros.selenified.Selenified;
import com.coveros.selenified.application.App;

import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class JenkinsTest extends Selenified{
    @BeforeClass(alwaysRun = true)
    public void beforeClass(ITestContext test) {
        // set the base URL for the tests here
        setTestSite(this, test, System.getProperty("ip") + ":8080");
    }
    @Test(groups = {"smoke", "jenkins"}, description = "Make sure jenkins is up")
    public void jenkinsHomeTest() {
        // use this object to manipulate the app
        App app = this.apps.get();
        // verify the correct page title
        app.azzert().titleEquals("Dashboard [Jenkins]");
        // verify no issues
        finish();
    }
}