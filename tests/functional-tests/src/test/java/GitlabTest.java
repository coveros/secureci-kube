import com.coveros.selenified.Selenified;
import com.coveros.selenified.application.App;

import org.testng.annotations.BeforeClass;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class GitlabTest extends Selenified{
    @BeforeClass(alwaysRun = true)
    public void beforeClass(ITestContext test) {
        // set the base URL for the tests here
        setTestSite(this, test, System.getProperty("ip"));
    }
    @Test(groups = {"smoke", "gitlab"}, description = "Make sure gitlab is up")
    public void gitlabHomeTest() {
        // use this object to manipulate the app
        System.out.println("IP: " + System.getProperty("ip"));
        App app = this.apps.get();
        try{
            Thread.sleep(100);
        }catch(Exception e ){
            
        }
        // verify the correct page title
        app.azzert().titleEquals("GitLab");
        // verify no issues
        finish();
    }
}